var gulp = require('gulp-help')(require('gulp'));
var plugins = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
var static_root = 'comiccollector/assets/';

var config = {
    src: {
        css: static_root + 'src/**/*.scss',
        copy: {
            js: [
                static_root + 'src/**/*.js',
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
                'node_modules/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
                'node_modules/metismenu/dist/metisMenu.min.js',
                'node_modules/jquery-slimscroll/jquery.slimscroll.min.js',
                'node_modules/pace/pace.min.js'
            ],
            fonts: [
                'node_modules/font-awesome/fonts/*',
                'node_modules/bootstrap-sass/assets/fonts/bootstrap/*'
            ],
            img: [
                static_root + 'src/img/**/*'
            ]
        }
    },
    dest: static_root + 'dist/',
    sass: {
        includePaths: ['node_modules']
    }
};

gulp.task('styles', function () {
    gulp.src(config.src.css)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass(config.sass).on('error', plugins.sass.logError))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.stream({match: '**/*.css'}))
});

gulp.task('copy', function () {
    gulp.src(config.src.copy.js)
        .pipe(plugins.copy(config.dest + 'js/', {'prefix': 4}));
    gulp.src(config.src.copy.fonts)
        .pipe(plugins.copy(config.dest + 'fonts/', {'prefix': 4}));
    gulp.src(config.src.copy.img)
        .pipe(plugins.copy(config.dest + 'img/', {'prefix': 4}));
});

gulp.task('watch', function () {
    browserSync.init({
        proxy: "localhost:8000"
    });
    gulp.watch(config.src.css, ['styles']);
});

gulp.task('default', ['styles', 'copy']);
