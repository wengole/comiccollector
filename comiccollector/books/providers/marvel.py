import hashlib
from datetime import datetime
from math import ceil

import isbnlib
import re
import requests
from dateutil import parser
from django.conf import settings

from ..models import Book, Series
from . import BaseBookInfoProvider


class MarvelBookInfoProvider(BaseBookInfoProvider):
    cache_key_prefix = 'marvel_comics'
    base_url = 'http://gateway.marvel.com/v1/public/'
    public_key = settings.MARVEL_PUBLIC_KEY
    private_key = settings.MARVEL_PRIVATE_KEY
    ts = None
    limit = 10

    @property
    def offset(self):
        return (self.page - 1) * self.limit

    def get_hash(self):
        return hashlib.md5(
                ('%s%s%s' % (
                    self.ts,
                    self.private_key,
                    self.public_key)).encode()).hexdigest()

    def get_query(self, **kwargs):
        self.ts = '%d' % int(datetime.now().timestamp())
        kwargs.update({
            'ts': self.ts,
            'apikey': self.public_key,
            'hash': self.get_hash(),
            'limit': self.limit,
            'offset': self.offset})
        return kwargs

    def do_request(self, query, resource='comics'):
        self.query_dict = self.get_query(**query)
        if self._raw_cached is None:
            self._raw_cached = requests.get(
                '%s%s' % (
                    self.base_url,
                    resource),
                params=self.query_dict)
        data = self._raw_cached.json()
        return data

    def search(self):
        self.query_dict = {
            'noVariants': 'true',
        }
        if self.isbn is None:
            self.query_dict['titleStartsWith'] = self.querystring
            data = self.do_request(self.query_dict)
            self.total = data['data']['total']
            self.pages = self.get_page_count(self.total)
            self.results = self.parse_results(data)
            self.mapping = self.raw_mapping(data)
            return self
        else:
            self.query_dict['isbn'] = isbnlib.mask(self.isbn, '-')
            data = self.do_request(self.query_dict)
            self.total = data['data']['total']
            self.results = self.parse_results(data)
            self.mapping = self.raw_mapping(data)
        return self

    def get(self, comic_id):
        data = self.do_request({}, resource='comics/%d' % comic_id)
        self.total = data['data']['total']
        self.pages = self.get_page_count(self.total)
        self.results = self.parse_results(data)
        self.mapping = self.raw_mapping(data)

    def get_page_count(self, total):
        return int(ceil(float(total) / float(self.limit)))

    def parse_results(self, results):
        books = []
        for result in results['data']['results']:
            dates = {x.get('type'): x for x in result.get('dates', {})}
            pub_date = dates.get('onsaleDate', {}).get('date', '')
            large_cover = '%s/portrait_incredible.%s' % (
                result.get('thumbnail', {}).get('path', ''),
                result.get('thumbnail', {}).get('extension', ''))
            medium_cover = '%s/portrait_medium.%s' % (
                result.get('thumbnail', {}).get('path', ''),
                result.get('thumbnail', {}).get('extension', ''))
            small_cover = '%s/portrait_small.%s' % (
                result.get('thumbnail', {}).get('path', ''),
                result.get('thumbnail', {}).get('extension', ''))
            texts = {x.get('type'): x for x in result.get('textObjects')}
            description = texts.get('issue_solicit_text', {}).get('text')
            books.append(Book(
                title=result['title'],
                isbn=result['isbn'],
                book_type=result.get('format', 'trade paperback').lower(),
                publication_date=parser.parse(pub_date).date(),
                large_cover=large_cover,
                medium_cover=medium_cover,
                small_cover=small_cover,
                description=description,
                level=1,
                page_count=result.get('pageCount', 0),
                marvel_id=result.get('id')))
        return books

    def raw_mapping(self, results):
        return {isbnlib.get_canonical_isbn(x['isbn']): x for x in results['data']['results']}


class MarvelSeriesInfoProvider(MarvelBookInfoProvider):
    cache_key_prefix = 'marvel_series'

    def search(self) -> 'MarvelSeriesInfoProvider':
        self.query_dict = self.get_query(titleStartsWith=self.querystring)
        data = self.do_request(self.query_dict, 'series')
        self.total = data['data']['total']
        self.results = self.parse_results(data)
        self.pages = self.get_page_count(self.total)
        return self

    def parse_results(self, results):
        series = []
        for result in results['data']['results']:
            end_year = result['endYear'] if result['endYear'] < 2099 else 9999
            name = result['title'].replace(re.search('\s?\([\s\w0-9\-]+\)', result['title']).group(0), '')
            series.append(Series(
                name=name,
                start_year=result['startYear'],
                end_year=end_year))
        return series
