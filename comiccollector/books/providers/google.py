from math import ceil

from django.conf import settings
from googleapiclient.discovery import build

from . import BaseBookInfoProvider
from ..models import Book


class GoogleBookInfoProvider(BaseBookInfoProvider):
    cache_key_prefix = 'google'

    @staticmethod
    def get_api():
        if settings.GOOGLE_DEV_KEY is not None:
            service = build(
                    'books',
                    'v1',
                    developerKey=settings.GOOGLE_DEV_KEY)
        else:
            service = build('books', 'v1')
        return service

    def search(self):
        service = self.get_api()
        if self.isbn is not None:
            query = 'isbn: %s' % self.isbn
        else:
            query = self.querystring
        req = service.volumes().list(q=query)
        results = req.execute()
        return {
            'total': results.get('totalItems', 1),
            'pages': ceil(results['totalItems'] / 10),
            'results': self.parse_results(results)}

    def parse_results(self, results):
        results_out = {}
        for result in results.get('items', []):
            attrs = result.get('volumeInfo', {})
            images = attrs.get('imageLinks', {})
            isbn = [x['identifier'] for x in attrs['industryIdentifiers'] if
                    x.get('type') == 'ISBN_13'][0]
            results_out[isbn] = Book(
                    title=attrs.get('title'),
                    author=', '.join(attrs.get('authors', [])),
                    isbn=isbn,
                    publication_date=attrs.get('publishedDate'),
                    small_image=images.get('smallThumbnail'),
                    medium_image=images.get('thumbnail'),
                    description=attrs.get('description'))
        return results_out
