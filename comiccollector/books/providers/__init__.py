import hashlib
import json

import isbnlib
from django.core.cache import cache
from django.core.handlers.wsgi import WSGIRequest
from pure_pagination import Paginator
from pure_pagination.paginator import Page

PROVIDER_FIELDS = (
    'title',
    'isbn',
    'book_type',
    'format',
    'publication_date',
    'amazon_link',
    'large_cover',
    'medium_cover',
    'small_cover',)


class BaseBookInfoProvider(object):
    cache_key_prefix = ''
    querystring = ''
    query_dict = {}
    isbn = ''
    page = 1
    cache_duration = 86400
    pages = 0
    total = 0
    results = []
    mapping = {}

    @property
    def cache_key(self):
        d = {
            'querystring': self.querystring,
            'isbn': self.isbn,
        }
        d.update(self.query_dict)
        return '%s_result_%s_%d' % (
            self.cache_key_prefix,
            hashlib.new(
                'sha256',
                json.dumps(d, sort_keys=True).encode('utf-8')).hexdigest(),
            self.page)

    @property
    def cached(self):
        return cache.get(self.cache_key)

    @cached.setter
    def cached(self, value):
        cache.set(self.cache_key, value, self.cache_duration)

    @property
    def _raw_cached(self):
        return cache.get('raw_%s' % self.cache_key)

    @_raw_cached.setter
    def _raw_cached(self, value):
        cache.set('raw_%s' % self.cache_key, value, self.cache_duration)

    @property
    def isbns(self):
        return cache.get('isbns_%s' % self.isbn)

    @isbns.setter
    def isbns(self, value):
        cache.set('isbns_%s' % self.isbn, value, self.cache_duration)

    def query(self, querystring: str, page: int =1) -> 'BaseBookInfoProvider':
        self.querystring = querystring
        self.page = page
        self.isbn = isbnlib.get_canonical_isbn(self.querystring)
        if self.isbn is not None and self.isbns is None:
            self.isbns = isbnlib.editions(self.isbn, service='thingl')
        if self.cached is not None:
            return self.cached
        self.cached = self.search()
        return self.cached

    def search(self) -> 'BaseBookInfoProvider':
        """
        Interact with API, parse and process results
        :return: self
        """
        raise NotImplementedError

    def parse_results(self, results):
        """
        Parse API results into a list of :class:`Book` models
        :param results: Results from API loosely parsed to Python objects
        :type results: varies
        :return: List of :class:`Book`s
        :rtype: list
        """
        raise NotImplementedError

    def raw_mapping(self, results):
        raise NotImplementedError


class ProviderPaginator(Paginator):

    def __init__(self, object_list: list, per_page: int, orphans: int =0, allow_empty_first_page: bool =True,
                 request: WSGIRequest =None, provider: BaseBookInfoProvider =None) -> None:
        super().__init__(object_list, per_page, orphans, allow_empty_first_page, request)
        self.provider = provider

    def page(self, number: int) -> Page:
        number = self.validate_number(number)
        if number != self.provider.page:
            results = self.provider.query(
                self.provider.querystring,
                page=number).results
        else:
            results = self.provider.results
        return Page(results, number, self)

    def _get_num_pages(self) -> int:
        return self.provider.pages
    num_pages = property(_get_num_pages)

    def _get_count(self) -> int:
        return self.provider.total
    count = property(_get_count)
