import bottlenose
import isbnlib
import xmltodict
from dateutil import parser
from django.conf import settings
from django.core.cache import cache

from . import BaseBookInfoProvider
from ..models import Book

BOOKS_NODE_ID = '1025612'


class AmazonBookInfoProvider(BaseBookInfoProvider):
    cache_key_prefix = 'amazon'
    departments_cache_key = 'amazon_books_departments'

    @staticmethod
    def get_api():
        amazon = bottlenose.Amazon(
            settings.AWS_ACCESS_KEY_ID,
            settings.AWS_SECRET_ACCESS_KEY,
            settings.AWS_ASSOC_TAG,
            Region='UK')
        return amazon

    @property
    def departments(self):
        cached = cache.get(self.departments_cache_key)
        if cached is not None:
            return cached
        resp = self.get_api().BrowseNodeLookup(BrowseNodeId=BOOKS_NODE_ID)
        departments = {
            x['BrowseNodeId']: x['Name']
            for x in xmltodict.parse(resp)['BrowseNodeLookupResponse']
            ['BrowseNodes']['BrowseNode']['Children']['BrowseNode']}
        cache.set(self.departments_cache_key, departments, 86400)
        return departments

    def get(self, isbn):
        self.isbn = isbnlib.get_canonical_isbn(isbn)
        self.isbns = [self.isbn]
        if self.cached is not None:
            return self.cached
        self.cached = self.search()
        return self.cached

    def search(self):
        api = self.get_api()
        self.query_dict = {
            'SearchIndex': 'Books',
            'ResponseGroup': 'ItemAttributes,Images,EditorialReview,'
                             'BrowseNodes',
            'MerchantId': 'Amazon',
            'BrowseNode': 274081,
        }

        if self.isbn is None:
            self.query_dict.update({
                'Power': (
                    'keywords: %s binding: not kindle not comic' %
                    self.querystring),
                'ItemPage': self.page
            })
            if self._raw_cached is None:
                self._raw_cached = api.ItemSearch(**self.query_dict)
            self.total, self.pages, self.results, self.mapping = self.process_response(self._raw_cached)
        else:
            self.query_dict.update({
                'Power': 'binding: not kindle not comic',
                'ItemId': self.isbn,
                'IdType': 'ISBN'
            })
            self.results = []
            for isbn in self.isbns:
                self.query_dict['ItemId'] = isbn
                if self._raw_cached is None:
                    self._raw_cached = api.ItemLookup(**self.query_dict)
                t, p, r, m = self.process_response(self._raw_cached)
                self.total += t
                self.pages = 1 if p > 0 else 0
                self.results.extend(r)
                self.mapping.update(m)
        return self

    def process_response(self, resp):
        query_results = xmltodict.parse(resp).popitem()[1]['Items']
        total = int(query_results.get('TotalResults', '1'))
        pages = int(query_results.get('TotalPages', '1'))
        results = self.parse_results(query_results)
        mapping = self.raw_mapping(query_results)
        return total, pages, results, mapping

    def parse_results(self, results):
        results_out = []
        items = self.get_items(results)
        for result in items:
            attrs = result.get('ItemAttributes')
            book_type = attrs.get('Binding').lower()
            if book_type == 'paperback':
                book_type = 'trade paperback'
            isbn = attrs.get('ISBN', '')
            results_out.append(Book(
                title=attrs.get('Title'),
                isbn=isbnlib.to_isbn13(isbn),
                book_type=book_type,
                publication_date=parser.parse(attrs.get('PublicationDate')).date(),
                amazon_link=result.get('DetailPageURL', ''),
                large_cover=result.get('LargeImage', {}).get('URL', ''),
                medium_cover=result.get('MediumImage', {}).get('URL', ''),
                small_cover=result.get('SmallImage', {}).get('URL', '')))
        return results_out

    @staticmethod
    def get_items(results):
        items = results.get('Item', [])
        if not isinstance(items, list):
            items = [items]
        return items

    def raw_mapping(self, results):
        mapping = {}
        for item in self.get_items(results):
            attrs = item['ItemAttributes']
            isbn = isbnlib.to_isbn13(attrs.get('ISBN', attrs.get('EISBN', '')))
            mapping[isbn] = item
        return mapping
