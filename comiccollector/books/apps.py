from django.apps import AppConfig


class BooksConfig(AppConfig):
    name = 'books'

    def ready(self):
        from watson import search as watson
        Book = self.get_model('Book')
        watson.register(Book)
