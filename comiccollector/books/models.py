import isbnlib
from django.conf import settings
from django.contrib.postgres.fields import HStoreField
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from imagekit import ImageSpec, register
from imagekit.models import ImageSpecField
from imagekit.utils import get_field_info
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from pilkit import processors

from .choices import book_types, image_source

YEARS = [(x, x) for x in range(1950, now().year + 1)]
YEARS.append((9999, 'ongoing'))


class Book(MPTTModel):
    """
    Book Model
    """
    title = models.TextField(
        verbose_name='Title')
    isbn = models.CharField(
        verbose_name='ISBN',
        max_length=30,
        unique=True)
    publication_date = models.DateField(
        verbose_name='Publication Date',
        null=True,
        blank=True)
    author_artist = models.ManyToManyField(
        blank=True,
        to='AuthorArtist',
        verbose_name='Author/Artists')
    issues = models.ManyToManyField(
        to='Issue',
        related_name='books',
        blank=True)
    series = models.ManyToManyField(
        to='Series',
        related_name='books',
        blank=True)
    series_mapping = HStoreField(
        default=dict(),
        blank=True)
    number = models.PositiveIntegerField(
        verbose_name='Volume number',
        help_text='Volume number if the book is part of a series of volumes',
        null=True,
        blank=True)
    book_type = models.CharField(
        max_length=25,
        verbose_name='Book Type',
        help_text='Type of book (such as TPB or Omnibus)',
        choices=book_types,
        default=book_types[0][0])
    description = models.TextField(
        help_text='Book description',
        blank=True)
    large_cover = models.URLField(
        max_length=1024,
        help_text='URL to large cover image',
        blank=True)
    medium_cover = models.URLField(
        max_length=1024,
        help_text='URL to medium cover image',
        blank=True)
    small_cover = models.URLField(
        max_length=1024,
        help_text='URL to small cover image',
        blank=True)
    amazon_link = models.URLField(
        max_length=1024,
        help_text='Amazon detail page',
        blank=True)
    previous = TreeForeignKey(
        to='self',
        help_text='The previous book in ongoing story',
        related_name='next',
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    marvel_id = models.PositiveIntegerField(
        verbose_name='Marvel ID',
        blank=True,
        null=True,
        unique=True)
    page_count = models.PositiveIntegerField(
        blank=True,
        null=True)

    @property
    def related_books(self):
        same_series = Q(series__id__in=self.series.values('id'))
        children = Q(id__in=self.get_children().values('id'))
        parent = Q(id=self.previous.id) if self.previous else Q()
        return Book.objects.filter(same_series | children | parent).exclude(id=self.id).order_by('?')[:12]

    def image_tag(self, size='large'):
        attr = '{}_cover'.format(size)
        src = getattr(self, attr, '')
        if src == '':
            src = self.large_cover
        return mark_safe(
            '<img src="{src}" class="{size} img-responsive" />'.format(
                src=src,
                size=size)
        )
    image_tag.short_description = 'Cover image'

    def thumbnail(self):
        return self.image_tag(size='small')

    def preview_image(self):
        return self.image_tag(size='medium')

    def amazon_button(self):
        return mark_safe(
            '<a href="{.amazon_link}" class="amazon-button" target="_blank">'
            '<i class="fa fa-amazon"></i> Amazon Page</a>'.format(self))
    amazon_button.short_description = 'Amazon page'

    def isbn_mask(self):
        return isbnlib.mask(self.isbn, '-')

    def has_description(self):
        return self.description not in ('', None)
    has_description.boolean = True

    @property
    def issue_count(self):
        return self.issues.count()

    def has_previous(self):
        return self.previous is not None
    has_previous.boolean = True

    def has_next(self):
        return self.get_children().count() > 0
    has_next.boolean = True

    def last(self):
        if self.has_next():
            return self.get_descendants()[self.get_descendant_count() - 1]

    def get_absolute_url(self):
        return reverse('comic-detail', kwargs={'book_id': self.id})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.isbn = isbnlib.canonical(self.isbn)
        return super().save(
            force_insert,
            force_update,
            using,
            update_fields)

    def __str__(self):
        return self.title

    class Meta(object):
        permissions = (
            ('view_book', 'Can view and search for books'),
        )

    class MPTTMeta(object):
        parent_attr = 'previous'


class AuthorArtist(models.Model):
    """
    Simple model of an Author, Artist etc.
    """
    name = models.TextField(
        verbose_name='Author or Artist Name')
    role = models.CharField(
        max_length=50,
        blank=True)

    def __str__(self):
        return '{0.name} ({0.role})'.format(self)

    class Meta(object):
        verbose_name = 'Author or Artist'
        verbose_name_plural = 'Authors and Artists'
        unique_together = ('name', 'role')


class Issue(models.Model):
    """
    Although technically a book in real life, for simplicity this is a basic
    model that just tracks an issue number with a :class:`series<Series>`
    """
    number = models.PositiveIntegerField(
        verbose_name='Issue Number')
    suffix = models.CharField(
        max_length=25,
        verbose_name='Issue number suffix',
        blank=True)
    series = models.ForeignKey(
        to='Series',
        related_name='issues')

    class Meta(object):
        ordering = ['series__name', 'number', 'suffix']
        unique_together = ('number', 'suffix', 'series')

    def __str__(self):
        suffix = '.%s' % self.suffix if self.suffix else ''
        return '%s #%d%s' % (self.series.name, self.number, suffix)


class Series(models.Model):
    """
    A very simple model with just the name of the series
    """
    name = models.TextField(unique=True)
    start_year = models.IntegerField(
        choices=YEARS,
        default=1950)
    end_year = models.IntegerField(
        choices=YEARS,
        default=9999)

    class Meta(object):
        verbose_name_plural = 'Series'
        ordering = ['name']

    def __html__(self):
        return self.__str__()

    def __str__(self):
        return mark_safe('%s (%s &mdash; %s)' % (
            self.name,
            self.get_start_year_display(),
            self.get_end_year_display()))


class Event(models.Model):
    """
    Another simple model with just the name of the event
    """
    name = models.TextField(
        verbose_name='Event name',
        unique=True)
    books = models.ManyToManyField(
        to='Book',
        verbose_name='Books',
        related_name='events',
        blank=True)

    def __str__(self):
        return self.name


class Collection(models.Model):
    """
    Collection model to keep a user's owned, wanted, read etc books
    """
    user = models.OneToOneField(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='collection')
    books = models.ManyToManyField(
        to='Book',
        through='CollectedBooks')

    def in_collection(self, book: Book) -> models.query.QuerySet:
        return CollectedBooks.objects.filter(book=book, collection=self)

    def get_state(self, book: Book) -> str:
        in_collection = self.in_collection(book)
        if in_collection.count() > 0:
            return in_collection.first().state
        return 'unowned'


class CollectedBooks(models.Model):
    collection = models.ForeignKey(
        to='Collection',
        related_name='collected_books')
    book = models.ForeignKey(
        to='Book',
        related_name='collections')
    owned = models.BooleanField(
        default=True)
    read = models.BooleanField(
        default=False)

    @property
    def state(self) -> str:
        state = 'wanted'
        if self.read:
            state = 'read'
        elif self.owned:
            state = 'owned'
        return state

    class Meta(object):
        unique_together = ('collection', 'book')
        verbose_name_plural = 'Collected Books'


class BookImage(models.Model):
    upload_path = 'img/books'
    thumbnail_width = 130
    preview_width = 210
    book = models.ForeignKey(
        'Book',
        related_name='images')
    source = models.CharField(
        max_length=20,
        choices=image_source,
        default=image_source[0][0])
    image = models.ImageField(
        upload_to=upload_path,
        max_length=1024,
        width_field='width',
        height_field='height')
    width = models.PositiveIntegerField(
        default=0)
    height = models.PositiveIntegerField(
        default=0)
    image_thumbnail = ImageSpecField(
        source='image',
        processors=[
            processors.Thumbnail(thumbnail_width)
        ])
    image_preview = ImageSpecField(
        source='image',
        processors=[
            processors.Thumbnail(preview_width)
        ])
    cropped_thumbnail = ImageSpecField(
        source='image',
        processors=[
            processors.Thumbnail(thumbnail_width),
            processors.Crop(thumbnail_width, thumbnail_width, anchor=processors.Anchor.TOP)
        ])
    cropped_preview = ImageSpecField(
        source='image',
        processors=[
            processors.Thumbnail(preview_width),
            processors.Crop(preview_width, preview_width, anchor=processors.Anchor.TOP)
        ])
    original_cropped = ImageSpecField(
        source='image',
        id='books:book:squared_original')


class SquaredOriginal(ImageSpec):

    @property
    def processors(self):
        model, field_name = get_field_info(self.source)
        return [processors.Crop(model.width, model.width, anchor=processors.Anchor.TOP)]
register.generator('books:book:squared_original', SquaredOriginal)
