from dal import autocomplete
from django import forms

from .choices import collection_actions
from .models import Book, Issue, Series


class SearchForm(forms.Form):
    q = forms.CharField(
        max_length=1024,
        widget=forms.TextInput(attrs={'id': 'searchbar'}))


class BookAdminForm(forms.ModelForm):
    isbn = forms.CharField(
        max_length=30,
        required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.fields.get('previous') is not None:
            self.fields['previous'].level_indicator = ''

    class Meta(object):
        model = Book
        fields = '__all__'
        widgets = {
            'author_artist': autocomplete.ModelSelect2Multiple(
                url='author-autocomplete',
                attrs={'data-width': 'resolve'}),
            'issues': autocomplete.ModelSelect2Multiple(
                url='issue-autocomplete',
                attrs={'data-width': 'resolve'}),
            'cover_image': forms.HiddenInput(),
            'amazon_link': forms.HiddenInput(),
            'previous': autocomplete.ModelSelect2(
                url='book-autocomplete',
                attrs={'data-width': 'resolve'}),
            'series': autocomplete.ModelSelect2Multiple(
                url='series-autocomplete',
                attrs={'data-width': 'resolve'}),
            'title': forms.TextInput(),
        }


class IssueForm(forms.ModelForm):
    series = forms.ModelChoiceField(
        Series.objects.all(),
        label='Series',
        widget=autocomplete.ModelSelect2(
            url='series-autocomplete',
            attrs={'data-width': 'resolve'})
    )
    number = forms.IntegerField(
        label='Number',
        min_value=0)
    suffix = forms.CharField(
        label='Suffix',
        max_length=10,
        strip=True,
        required=False)

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        if instance is not None:
            self.fields['series'].initial = instance.issue.series
            self.fields['number'].initial = instance.issue.number
            self.fields['suffix'].initial = instance.issue.suffix

    def save(self, commit=True):
        super().save(commit)
        if self.instance.id is None:
            issue, _ = Issue.objects.get_or_create(
                series=self.cleaned_data['series'],
                number=self.cleaned_data['number'],
                suffix=self.cleaned_data['suffix'])
            self.instance.issue_id = issue.id
        else:
            for f_name in self.changed_data:
                setattr(self.instance.issue, f_name, self.cleaned_data[f_name])
            self.instance.issue.save()
        return self.instance

    class Meta(object):
        model = Issue.books.through
        fields = ('series', 'number', 'suffix')
        widgets = {
            'series': autocomplete.ModelSelect2(
                url='series-autocomplete',
                attrs={'data-width': 'resolve'}),
        }


class SeriesForm(forms.ModelForm):
    name = forms.CharField(max_length=255)
    start_year = forms.IntegerField(min_value=1950, max_value=9999)
    end_year = forms.IntegerField(min_value=1950, max_value=9999)

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        if instance is not None:
            self.fields['name'].initial = instance.series.name
            self.fields['start_year'].initial = instance.series.start_year
            self.fields['end_year'].initial = instance.series.end_year

    def save(self, commit=True):
        super().save(commit)
        if self.instance.id is None:
            series, _ = Series.objects.get_or_create(
                name=self.cleaned_data['name'],
                start_year=self.cleaned_data['start_year'],
                end_year=self.cleaned_data['end_year'])
            self.instance.series_id = series.id
        else:
            for f_name in self.changed_data:
                setattr(self.instance.series, f_name, self.cleaned_data[f_name])
            self.instance.series.save()
        return self.instance

    class Meta(object):
        model = Series.books.through
        fields = ('name', 'start_year', 'end_year')


class BookDetailForm(forms.Form):
    book = forms.ModelChoiceField(
        queryset=Book.objects.all(),
        widget=forms.HiddenInput)
    action = forms.ChoiceField(
        choices=collection_actions,
        widget=forms.HiddenInput)


class SeriesSearchForm(forms.Form):
    name = forms.CharField(max_length=255)
    original = forms.CharField(max_length=255)
    start_year = forms.IntegerField(
        max_value=9999,
        min_value=1950,
        required=False)
    end_year = forms.IntegerField(
        max_value=9999,
        min_value=1950,
        required=False)
