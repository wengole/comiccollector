from typing import Union

from django import template
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.utils.safestring import mark_safe
from watson.models import SearchEntry

from books.utilities import get_collection
from ..models import Book

register = template.Library()


@register.filter
def highlight(text, word):
    return mark_safe(text.replace(word, "<span class='highlight'>%s</span>" % word))


@register.simple_tag(takes_context=True)
def book_state(context: RequestContext, book: Book) -> str:
    user = context.request.user
    collection = get_collection(user)
    return collection.get_state(book)


@register.simple_tag
def normalise_book(book: Union[Book, SearchEntry]) -> str:
    if isinstance(book, SearchEntry):
        book = book.object
    return book


@register.simple_tag(takes_context=True)
def current_nav(context: RequestContext, urls: str) -> str:
    if context.request.path in (reverse(url) for url in urls.split()):
        return "active"
    return ""
