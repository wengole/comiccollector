# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-08 10:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0010_authorartist_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authorartist',
            name='name',
            field=models.TextField(verbose_name='Author or Artist Name'),
        ),
        migrations.AlterUniqueTogether(
            name='authorartist',
            unique_together=set([('name', 'role')]),
        ),
    ]
