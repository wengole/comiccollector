# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-29 14:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('books', '0006_auto_20160128_1350'),
    ]

    operations = [
        migrations.CreateModel(
            name='CollectedBooks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.CharField(choices=[('owned', 'Owned'), ('wanted', 'Wanted'), ('read', 'Read')], max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AlterField(
            model_name='book',
            name='previous',
            field=mptt.fields.TreeForeignKey(blank=True, help_text='The previous book in ongoing story', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='next', to='books.Book'),
        ),
        migrations.AddField(
            model_name='collection',
            name='books',
            field=models.ManyToManyField(through='books.CollectedBooks', to='books.Book'),
        ),
        migrations.AddField(
            model_name='collection',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='collectedbooks',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='books.Book'),
        ),
        migrations.AddField(
            model_name='collectedbooks',
            name='collection',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='books.Collection'),
        ),
    ]
