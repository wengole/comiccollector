# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-08 16:51
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0013_auto_20160208_1640'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='collectedbooks',
            options={'verbose_name_plural': 'Collected Books'},
        ),
        migrations.AlterUniqueTogether(
            name='collectedbooks',
            unique_together=set([('collection', 'book')]),
        ),
    ]
