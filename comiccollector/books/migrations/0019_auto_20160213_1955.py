# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-13 19:55
from __future__ import unicode_literals

from django.db import migrations


def split_issue_number(apps, schema_editor):
    Issue = apps.get_model('books', 'Issue')
    for issue in Issue.objects.all():
        parts = issue.number.split('.')
        issue.number = parts[0]
        try:
            issue.suffix = parts[1]
        except IndexError:
            pass
        issue.save()


def join_issue_number(apps, schema_editor):
    Issue = apps.get_model('books', 'Issue')
    for issue in Issue.objects.all():
        issue.number = '.'.join([issue.number, issue.suffix])
        issue.save()


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0018_auto_20160213_2004'),
    ]

    operations = [
        migrations.RunPython(split_issue_number, join_issue_number)
    ]
