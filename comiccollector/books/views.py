import isbnlib
import vanilla
from dal import autocomplete
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Value, When, Case, CharField
from django.shortcuts import get_object_or_404
from django.views import generic
from watson import search as watson

from books.utilities import get_collection
from . import utilities as utils
from .forms import BookDetailForm, SearchForm
from .models import Book, AuthorArtist, Issue, Collection, Series


class AuthorAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # TODO: permissions
        if not self.request.user.is_authenticated():
            return AuthorArtist.objects.none()
        qs = AuthorArtist.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class IssueAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # TODO: permissions
        if not self.request.user.is_authenticated():
            return Issue.objects.none()
        qs = Issue.objects.all()
        if self.q:
            qs = qs.filter(series__name__icontains=self.q)
        return qs


class BookAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Book.objects.none()
        qs = Book.objects.all()
        if self.q:
            qs = qs.filter(title__icontains=self.q)
        return qs


class SeriesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Series.objects.none()
        qs = Series.objects.all()
        if self.q:
            qs = qs.filter(name__icontains=self.q)
        return qs


class HomePageView(generic.TemplateView):
    template_name = 'home.html'


class BookActionMixin(object):
    object = None

    def form_valid(self, form):
        collection = self._get_collection()
        action = form.cleaned_data['action']
        book = form.cleaned_data['book']
        message = 'Success'
        succeeded = True
        if action == 'add-collection':
            succeeded, message = utils.add_book_to_collection(book, collection)
        elif action in ('remove-collection', 'remove-wishlist'):
            succeeded, message = utils.remove_book_from_collection(book, collection)
        elif action == 'add-wishlist':
            qs = Book.objects.filter(id=book.id)
            for book in utils.get_books_in_tree(collection, qs):
                succeeded, message = utils.add_book_to_collection(book, collection, owned=False)
        elif action == 'add-read':
            succeeded, message = utils.add_book_to_collection(book, collection, read=True)
        elif action == 'remove-read':
            succeeded, message = utils.add_book_to_collection(book, collection, read=False)
        level = messages.SUCCESS if succeeded else messages.ERROR
        messages.add_message(
            self.request,
            level=level,
            message=message)
        return super().form_valid(form)

    def _get_collection(self):
        user = self.request.user
        collection = utils.get_collection(user)
        return collection

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)


class BookDetailView(BookActionMixin, vanilla.DetailView, vanilla.FormView):
    model = Book
    form_class = BookDetailForm

    def get_object(self, queryset=None):
        book_id = self.kwargs.get('book_id', 0)
        return get_object_or_404(Book, id=book_id)

    def get_context_data(self, **kwargs):
        collection = self._get_collection()
        return super().get_context_data(
            state=collection.get_state(self.object),
            **kwargs)

    def get_author(self):
        writer = self.object.author_artist.filter(role__iexact='writer').first()
        return 'by {}'.format(writer.name) if writer is not None else ''

    def owned_issues(self):
        collection = self._get_collection()
        return collection.collected_books.exclude(
            book__id=self.object.id).filter(
            owned=True).values_list(
            'book__issues__id',
            flat=True)

    def form_invalid(self, form):
        message = list(form.errors.values())[0][0]
        messages.error(
            self.request,
            message='Failed. %s' % message)
        return super().form_invalid(form)

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER', reverse('comic-detail', kwargs=self.kwargs))


class SearchView(BookActionMixin, vanilla.ListView, vanilla.FormView):
    template_name = 'books/search_results.html'
    context_object_name = 'books'
    paginate_by = 12
    object_list = []

    def get_context_data(self, **kwargs):
        queries_without_page = self.request.GET.copy()
        if 'page' in queries_without_page:
            del queries_without_page['page']
        return super().get_context_data(
            queries=queries_without_page,
            **kwargs)

    def get_form_class(self):
        if self.request.POST.get('action', False):
            return BookDetailForm
        return SearchForm

    def get_form(self, data=None, files=None, **kwargs):
        if self.request.method == 'GET':
            data = self.request.GET
        return super().get_form(data, files, **kwargs)

    def get_queryset(self):
        form = self.get_form()
        if not form.is_valid():
            messages.error(
                self.request,
                'Please enter a valid search query')
            return []
        q = form.cleaned_data['q']
        isbn = isbnlib.get_canonical_isbn(q)
        self.object_list = Book.objects.none()
        if isbn is not None:
            self.object_list = Book.objects.filter(isbn=isbn)
        if isbn is None or self.object_list.count() < 1:
            self.object_list = watson.search(q)
        return self.object_list

    def post(self, request, *args, **kwargs):
        self.request = request
        form = self.get_form(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER', reverse('search'))


class UserProfileView(generic.UpdateView):
    model = get_user_model()
    fields = (
        'username',
        'first_name',
        'last_name',
    )

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('home-page')


class CollectionView(LoginRequiredMixin, SearchView):
    model = Collection
    template_name = 'books/collection.html'
    context_object_name = 'books'
    paginate_by = 12
    success_url = reverse_lazy('collection')

    def get_queryset(self):
        collection = get_collection(self.request.user)
        qs = collection.books.prefetch_related('images')
        return qs.annotate(
            state=Case(
                When(collections__read=True, then=Value('read')),
                When(collections__owned=True, then=Value('owned')),
                default=Value('wanted'),
                output_field=CharField()))
