from .author_artist import *
from .book import *
from .collection import *
from .event import *
from .issue import *
from .series import *


__all__ = [
    'BookAdmin',
    'AuthorArtistAdmin',
    'CollectionAdmin',
    'EventAdmin',
    'IssueAdmin',
    'SeriesAdmin',
]

admin_site = admin.site
admin_site.site_title = 'Comic Collector'
admin_site.site_header = 'Comic Collector'
admin_site.site_url = None
