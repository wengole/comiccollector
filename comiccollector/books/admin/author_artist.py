from django.contrib import admin

from ..models import AuthorArtist


@admin.register(AuthorArtist)
class AuthorArtistAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)
