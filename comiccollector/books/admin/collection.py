from django.contrib import admin

from ..models import Collection


class CollectedBooksInlineAdmin(admin.TabularInline):
    model = Collection.books.through


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ('user',)
    inlines = (CollectedBooksInlineAdmin,)
