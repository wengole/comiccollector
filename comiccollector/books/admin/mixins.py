from django.http import HttpResponseRedirect

from ..forms import SearchForm

IGNORED_PARAMS = [
    'provider',
]


class MediaMixin(object):
    class Media(object):
        css = {
            'all': (
                'css/books/extra_admin.css',
                'css/books/extra_admin.css.map',)
        }


class ProviderAttributeMixin(object):
    def __init__(self, request, *args, **kwargs):
        self.provider = request.GET.get('provider', 'db')
        super().__init__(request, *args, **kwargs)
        self.title = self.get_title(request)

    def get_title(self, request):
        if request.GET.get('provider', 'db') != 'db':
            return 'Select {.object_name}(s) to add to database'.format(self.model._meta)
        return 'Select {.object_name} to change'.format(self.model._meta)

    def get_filters_params(self, params=None):
        lookup_params = super().get_filters_params(params)
        for ignored in IGNORED_PARAMS:
            if ignored in lookup_params:
                del lookup_params[ignored]
        return lookup_params


class ActionInChangeFormMixin(object):
    def response_action(self, request, queryset):
        """
        Prefer http referer for redirect
        """
        response = super().response_action(
            request,
            queryset)
        return response

    def change_view(self, request, object_id, form_url='', extra_context=None):
        actions = self.get_actions(request)
        if actions:
            action_form = self.action_form(auto_id=None)
            action_form.fields['action'].choices = self.get_action_choices(request)
        else:
            action_form = None
        extra_context = extra_context or {}
        extra_context['action_form'] = action_form
        return super().change_view(request, object_id, form_url, extra_context=extra_context)


class CustomSearchMixin(object):
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        q = request.GET.get('q', '')
        form = SearchForm(initial={'q': q})
        extra_context['search_form'] = form
        return super().changelist_view(request, extra_context)


class RequestMixin(object):
    def changelist_view(self, request, extra_context=None):
        self.request = request
        return super().changelist_view(request, extra_context)


class ConditionalEditingMixin(object):
    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        self.provider = request.GET.get('provider', 'db')
        if self.provider != 'db':
            self.list_editable = ()
        else:
            self.list_editable = self._list_editable
        return super().changelist_view(request, extra_context)


class ComicCollectorAdminSearchMixin(CustomSearchMixin, MediaMixin):
    pass


class ComicCollectorAdminMixin(ConditionalEditingMixin, RequestMixin, ComicCollectorAdminSearchMixin, ActionInChangeFormMixin):
    pass
