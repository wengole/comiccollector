import json
from urllib.parse import urlencode

import isbnlib
from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth import get_user_model
from django.core import serializers
from django.core.checks import messages
from django.core.urlresolvers import reverse
from django.forms import inlineformset_factory
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from pure_pagination import PaginationMixin

from . import views as admin_views
from .actions import update_from_marvel
from .mixins import ComicCollectorAdminMixin, ProviderAttributeMixin
from ..forms import BookAdminForm, IssueForm, SeriesForm
from ..models import Book, Event
from ..providers import PROVIDER_FIELDS
from ..providers.amazon import AmazonBookInfoProvider
from ..providers.marvel import MarvelBookInfoProvider
from ..utilities import get_collection, add_book_to_collection


class EventsInline(admin.TabularInline):
    model = Event.books.through


class IssuesInline(admin.TabularInline):
    model = Book.issues.through
    formset = inlineformset_factory(Book, Book.issues.through, form=IssueForm, extra=0)
    form = IssueForm


class SeriesInline(admin.TabularInline):
    model = Book.series.through
    formset = inlineformset_factory(Book, Book.series.through, form=SeriesForm, extra=0)
    form = SeriesForm


class CollectionListFilter(admin.SimpleListFilter):
    title = 'collection'
    parameter_name = 'user'

    def lookups(self, request, model_admin):
        user = request.user
        collections = [(user.username, 'My collection')]
        # TODO: Not a great check
        if user.username != 'admin':
            return collections
        User = get_user_model()
        users = User.objects.exclude(username=user.username)
        return collections + [(x.username, x.get_full_name()) for x in users]

    def queryset(self, request, queryset):
        # TODO: Check permissions
        if self.value() is None:
            return queryset
        User = get_user_model()
        user = User.objects.get(username=self.value())
        collection = get_collection(user)
        return queryset.filter(id__in=collection.collected_books.values('book__id'))


class HasPreviousFilter(admin.SimpleListFilter):
    title = 'has previous'
    parameter_name = 'previous'

    def lookups(self, request, model_admin):
        return (
            (1, 'Yes'),
            (0, 'No')
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(previous__isnull=False)
        elif self.value() == '0':
            return queryset.filter(previous__isnull=True)
        return queryset


@admin.register(Book)
class BookAdmin(ComicCollectorAdminMixin, admin.ModelAdmin):
    list_display = (
        'thumbnail',
        'title',
        'number',
        'issue_count',
        'marvel_id',
        'has_description',
        'has_previous',
    )
    _list_editable = (
        'title',
        'number',)
    list_display_links = ('thumbnail',)
    list_filter = (
        'events',
        'series',
        CollectionListFilter,
        HasPreviousFilter,
    )
    search_fields = (
        'title',
        'description',
    )
    fields = (
        'preview_image',
        'title',
        'description',
        'page_count',
        'series',
        'number',
        'isbn',
        'publication_date',
        'author_artist',
        'book_type',
        'amazon_button',
        'previous',
        'marvel_id',
        'large_cover',
        'medium_cover',
        'small_cover',
        'amazon_link',
    )
    readonly_fields = (
        'image_tag',
        'amazon_button',
        'preview_image',
    )
    form = BookAdminForm
    list_per_page = 10
    actions = (
        update_from_marvel,)
    inlines = (
        EventsInline,
        IssuesInline,
        SeriesInline,
    )
    save_on_top = True
    date_hierarchy = 'publication_date'

    def get_search_results(self, request, queryset, search_term):
        qs, use_distinct = super().get_search_results(request, queryset, search_term)
        isbn = isbnlib.get_canonical_isbn(search_term)
        if isbn is not None:
            qs |= self.model.objects.filter(isbn=isbn)
        return qs, use_distinct

    def get_changelist(self, request, **kwargs):
        return BooksChangelist

    def get_list_display(self, request):
        if request.GET.get('provider', 'db') == 'db':
            return self.list_display
        return ('thumbnail',
                'title',
                'isbn',
                'book_type',
                'publication_date',
                'click_to_add')

    def get_list_display_links(self, request, list_display):
        if request.GET.get('provider', 'db') == 'db':
            return super().get_list_display_links(request, list_display)
        return

    def click_to_add(self, obj):
        Form = self.get_form(self.request, obj)
        form = Form(instance=obj)
        return mark_safe(
            render_to_string(
                'books/includes/quick_add_button.html',
                context={'form': form},
                request=self.request)
        )

    def get_urls(self):
        urls = super().get_urls()
        book_urls = [
            url(r'^quick_add/$',
                self.admin_site.admin_view(self.quick_add),
                name='book_quick_add'),
            url(r'^(?P<book_id>\d+)/(?P<field>\w+)/search-marvel/$',
                self.admin_site.admin_view(admin_views.search_marvel),
                name='search-marvel'),
            url(r'^update-marvel/(?P<book_id>\d+)/$',
                self.admin_site.admin_view(admin_views.UpdateBookFromMarvel.as_view()),
                name='update-marvel'),
            url(r'^update-series/(?P<book_id>\d+)/$',
                self.admin_site.admin_view(admin_views.UpdateBookSeries.as_view()),
                name='update-series'),
        ]
        return book_urls + urls

    def quick_add(self, request):
        owned = request.POST.get('quick-add', 'catalogue') == 'owned'
        Form = self.get_form(request)
        form = Form(data=request.POST)
        if form.is_valid():
            form.save()
            if owned:
                collection = get_collection(request.user)
                success, message = add_book_to_collection(form.instance, collection)
                level = messages.INFO if success else messages.WARNING
                self.message_user(
                    request,
                    message,
                    level)
            return redirect('admin:update-marvel', book_id=form.instance.id)
        else:
            self.message_user(
                request,
                'Failed to add "%s": %s' % (
                    form.cleaned_data['title'],
                    [x[0] for x in form.errors.values()][0]),
                level=messages.ERROR)
        return redirect(
            request.META.get('HTTP_REFERER', reverse('admin:books_book_changelist')))

    def get_changelist_form(self, request, **kwargs):
        if self.request.GET.get('provider', 'db') == 'db':
            return self.form
        return super().get_changelist_form(request, **kwargs)
    
    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['title'] = Book.objects.get(id=object_id).title
        return super().change_view(request, object_id, form_url, extra_context)


class BooksChangelist(PaginationMixin, ProviderAttributeMixin, ChangeList):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = self.opts.verbose_name_plural.title()

    def get_results(self, request):
        if self.provider == 'amazon':
            provider = AmazonBookInfoProvider()
        elif self.provider == 'marvel':
            provider = MarvelBookInfoProvider()
        else:
            return super().get_results(request)

        provider.query(
            querystring=request.GET.get('q'),
            page=int(request.GET.get('p', '0')) + 1)
        self.request = request
        paginator = self.get_paginator(provider.results, self.list_per_page)
        total = provider.total
        pages = provider.pages
        paginator._count = total
        paginator._num_pages = pages
        self.result_list = provider.results
        self.result_count = paginator.count
        self.full_result_count = total
        self.multi_page = self.result_count > self.list_per_page
        self.can_show_all = False
        self.paginator = paginator
        self.show_admin_actions = True

    def url_for_result(self, result):
        if self.provider == 'db':
            return super().url_for_result(result)
        json_str = serializers.serialize(
            'json',
            [result],
            fields=PROVIDER_FIELDS)
        params = json.loads(json_str)[0]['fields']
        return '%s?%s' % (
            reverse('admin:book_quick_add'),
            urlencode(params))
