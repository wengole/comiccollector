from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils.safestring import mark_safe

from .. import utilities as utils


def add_as_owned(model_admin, request, queryset):
    collection = utils.get_collection(request.user)
    added = 0
    for book in queryset:
        succeeded, message = utils.add_book_to_collection(book, collection)
        if succeeded:
            added += 1
        else:
            messages.error(
                request,
                message=message)
    if added > 0:
        messages.add_message(
            request,
            message='Added %d books to your collection' % added,
            level=messages.INFO)


add_as_owned.short_description = 'Add book(s) as owned'


def add_as_wanted(model_admin, request, queryset):
    added = 0
    collection = utils.get_collection(request.user)
    books_to_add = utils.get_books_in_tree(collection, queryset)
    for book in books_to_add:
        succeeded, message = utils.add_book_to_collection(book, collection, owned=False)
        if succeeded:
            added += 1
    messages.success(
        request,
        message='Added %d books to your collection as "wanted"' % added)


add_as_wanted.short_description = 'Add book(s) as wanted'


def update_from_marvel(model_admin, request, queryset):
    if queryset.count() == 1:
        return redirect('admin:update-marvel', book_id=queryset.first().id)
    for book in queryset:
        utils.update_book_from_marvel(request, book=book)
        try:
            issues = utils.get_collected_issues(book)
        except ValueError as ex:
            messages.error(
                request,
                message=ex.args[0])
            continue
        series_names = [
            series.group('series_name').strip()
            for series in issues
            ]
        all_found, _ = utils.check_series_exist(book, series_names)
        if not all_found:
            url = reverse('admin:update-series', kwargs={'book_id': book.id})
            messages.warning(
                request,
                message=mark_safe(
                    'Failed to get all series for <a href="{url}">"{book.title}"</a>'.format(
                        url=url,
                        book=book)))
    messages.add_message(
        request,
        level=messages.INFO,
        message='Updated %d books' % queryset.count())


update_from_marvel.short_description = 'Update book(s) from Marvel API'


def remove_from_collection(model_admin, request, queryset):
    collection = utils.get_collection(request.user)
    queryset = queryset.filter(id__in=collection.collected_books.values('book__id'))
    for book in queryset:
        utils.remove_book_from_collection(book, collection)
    messages.success(
        request,
        message='Removed %d books from collection' % queryset.count())
