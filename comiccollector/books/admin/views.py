from datetime import datetime

import vanilla
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect
from pure_pagination import PaginationMixin

from books import utilities as utils
from books.forms import BookAdminForm, SeriesSearchForm
from books.models import Book, Series
from books.providers import ProviderPaginator
from books.providers.marvel import MarvelSeriesInfoProvider, MarvelBookInfoProvider


class UpdateBookFromMarvel(PaginationMixin, UserPassesTestMixin, vanilla.ListView, vanilla.FormView):
    template_name = 'books/marvel_update_results.html'
    context_object_name = 'books'
    form_class = BookAdminForm
    all_found = True
    object = None
    provider = None
    paginate_by = 10

    def test_func(self):
        return self.request.user.is_authenticated and self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'q' in self.request.GET:
            self.provider = utils.get_marvel_books(
                self.object,
                querystring=self.request.GET['q'])
        else:
            self.provider = utils.get_marvel_books(self.object)
        return super().get(request, *args, **kwargs)

    def get_paginator(self, queryset, page_size, **kwargs):
        return ProviderPaginator(queryset, page_size, provider=self.provider, request=self.request)

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            title='Marvel API matches',
            **kwargs
        )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_object(self):
        return get_object_or_404(Book, id=self.kwargs['book_id'])

    def get_queryset(self):
        return self.provider.results

    def get_form(self, data=None, files=None, **kwargs):
        return super().get_form(
            data=data,
            instance=self.object)

    def form_valid(self, form):
        self.object.refresh_from_db()
        action = self.request.POST.get('action', 'missing')
        if form.cleaned_data.get('marvel_id') is not None:
            self.object.marvel_id = form.cleaned_data['marvel_id']
        for field in self.object._meta.get_fields():
            if not field.concrete or field.auto_created:
                continue
            old_value = getattr(self.object, field.name, None)
            new_value = form.cleaned_data.get(field.name, None)
            if (old_value in ('', None) or action == 'all') and new_value not in (None, ''):
                setattr(self.object, field.name, new_value)
        utils.update_field_from_marvel(
            book=self.object,
            marvel_result=utils.get_marvel_book(self.object),
            field='author_artist')
        self.object.save()
        return redirect('admin:update-series', book_id=self.object.id)

    def form_invalid(self, form):
        messages.error(
            self.request,
            message='Failed to update "{}"'.format(self.object.title))
        return redirect('admin:books_book_change', self.object.id)


class UpdateBookSeries(UserPassesTestMixin, vanilla.ListView, vanilla.FormView):
    form_class = SeriesSearchForm
    all_found = True
    template_name = 'books/series_matches.html'
    object = None
    context_object_name = 'book'

    def test_func(self):
        return self.request.user.is_authenticated and self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            issues = utils.get_collected_issues(self.object)
        except ValueError as ex:
            messages.error(
                self.request,
                message=ex.args[0])
            return redirect('admin:books_book_change', self.object.id)
        series_names = [
            series.group('series_name').strip()
            for series in issues
            ]
        self.all_found, series_data = utils.check_series_exist(self.object, series_names)
        if self.all_found:
            success, message = utils.parse_description(self.object)
            level = messages.SUCCESS if success else messages.WARNING
            messages.add_message(
                self.request,
                level,
                message)
            return redirect('admin:books_book_change', self.object.id)
        return self.render_to_response(
            self.get_context_data(
                series_data=series_data,
                collecting=utils.get_collecting(self.object),
                title='Series found for "{}"'.format(self.object.title)))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_template_names(self):
        if self.request.method == 'POST':
            return 'books/series_results.html'
        return super().get_template_names()

    def get_object(self):
        return get_object_or_404(Book, id=self.kwargs['book_id'])

    def form_valid(self, form):
        original_upper = form.cleaned_data['original'].upper()
        if 'add' in self.request.POST:
            series, _ = Series.objects.get_or_create(
                name=form.cleaned_data['name'],
                defaults={
                    'start_year': form.cleaned_data.get('start_year'),
                    'end_year': form.cleaned_data.get('end_year')})
            utils.add_series_to_book(self.object, series, original_upper)
            return redirect('admin:update-series', book_id=self.object.id)
        series_name = form.cleaned_data['name']
        series = Series.objects.filter(id=self.object.series_mapping.get(series_name.upper()))
        if form.cleaned_data['start_year'] is not None:
            start_year = int(form.cleaned_data['start_year'])
            series.filter(start_year=start_year)
        else:
            start_year = datetime.now().year
        if series.count() == 1:
            utils.add_series_to_book(self.object, series[0], original_upper)
            return redirect('admin:update-series', book_id=self.object.id)
        provider = MarvelSeriesInfoProvider()
        results = provider.query(
            querystring=series_name)
        if results.total == 0:
            series, _ = Series.objects.get_or_create(
                name=series_name.title(),
                defaults={
                    'start_year': start_year})
            utils.add_series_to_book(self.object, series, original_upper)
            return redirect('admin:update-series', book_id=self.object.id)
        return self.render_to_response(self.get_context_data(
            original=form.cleaned_data['original'],
            results=results.results,
            title='Search results for "{}"'.format(series_name)))


def search_marvel(request, book_id, field):
    book = get_object_or_404(Book, id=int(book_id))
    provider = MarvelBookInfoProvider()
    if book.marvel_id:
        provider.get(book.marvel_id)
    if provider.total < 1 and book.isbn:
        provider.query(book.isbn)
    if provider.total < 1:
        provider.query(book.title.split('(')[0].strip())
    if provider.total < 1:
        messages.add_message(request, messages.ERROR, 'Failed to "%s" on Marvel API' % book.title)
    else:
        marvel_result = list(provider.mapping.values())[0]
        succeeded = utils.update_field_from_marvel(book, marvel_result, field)
        if not succeeded:
            level = messages.INFO if succeeded else messages.ERROR
            messages.add_message(request, level, 'Failed to update %s from Marvel' % field)
    return redirect(
        request.META.get('HTTP_REFERER', reverse('admin:books_book_changelist')))
