import json
from urllib.parse import urlencode

from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.core import serializers
from django.core.urlresolvers import reverse

from ..admin.mixins import ComicCollectorAdminSearchMixin, ProviderAttributeMixin
from ..models import Series
from ..providers.marvel import MarvelSeriesInfoProvider


@admin.register(Series)
class SeriesAdmin(ComicCollectorAdminSearchMixin, admin.ModelAdmin):
    search_fields = ('name',)

    def get_changelist(self, request, **kwargs):
        return SeriesChangelist


class SeriesChangelist(ProviderAttributeMixin, ChangeList):
    def get_results(self, request):
        if self.provider != 'marvel':
            return super().get_results(request)
        provider = MarvelSeriesInfoProvider()
        results = provider.query(
            querystring=request.GET.get('q'),
            page=int(request.GET.get('p', '0')) + 1)
        paginator = self.model_admin.get_paginator(request, results.results)
        total = results.total
        pages = results.pages
        paginator._count = total
        paginator._num_pages = pages
        self.result_list = results.results
        self.result_count = paginator.count
        self.full_result_count = total
        self.multi_page = self.result_count > self.list_per_page
        self.can_show_all = False
        self.paginator = paginator
        self.show_admin_actions = True

    def url_for_result(self, result):
        if self.provider == 'db':
            return super().url_for_result(result)
        json_str = serializers.serialize(
            'json',
            [result],
            fields=('name', 'start_year', 'end_year'))
        params = json.loads(json_str)[0]['fields']
        return '%s?%s' % (
            reverse('admin:books_series_add'),
            urlencode(params))
