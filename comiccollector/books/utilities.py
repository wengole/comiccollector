import re
from typing import Iterator, Tuple
from urllib.parse import urlencode

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from .models import AuthorArtist, Collection, Issue, Series, CollectedBooks, Book
from .providers.marvel import MarvelBookInfoProvider

FIELD_MAPPING = {
    'page_count': 'pageCount',
    'marvel_id': 'id'
}
MARVEL_FIELDS = ('marvel_id', 'description', 'author_artist', 'page_count')

FULL_REGEX = (
    r'(?P<series>(?P<series_name>\b[\.\:\'&\-\w\s]+)(\((?P<start_year>\d+)\))?) #?((?P<start_prefix>\d+)\.'
    r'(?P<start_suffix>\w+)|(?P<start>\d+))-?((?P<end_prefix>\d+)\.(?P<end_suffix>\w+)|(?P<end>\d+))?'
)


def update_field_from_marvel(book: Book, marvel_result: dict, field: str, force: bool = False) -> bool:
    """
    Given a raw result from Marvel API, update the given field on the given Book
    :param force: Whether to overwrite the existing value
    :param book: `Book` object
    :param marvel_result: dict of raw result from Marvel API for the given book
    :param field: name of the field to update
    :return: `True` if successfully updated `False` if Marvel data was empty or missing
    """
    old_value = getattr(book, field)
    if marvel_result is None:
        return False
    if field != 'author_artist':
        new_value = marvel_result.get(FIELD_MAPPING.get(field, field), '')
    else:
        creators = marvel_result.get('creators', {}).get('items', [])
        for creator in creators:
            author, _ = AuthorArtist.objects.get_or_create(
                name=creator['name'],
                role=creator['role'])
            book.author_artist.add(author)
        book.save()
        return True
    if field == 'marvel_id' and isinstance(new_value, int):
        if Book.objects.filter(marvel_id=new_value).count() > 0:
            return False
    if new_value not in (None, '') and (force or old_value in (None, '')):
        setattr(book, field, new_value)
        book.save()
        return True
    return False


def get_marvel_books(book, querystring=None, page=1):
    provider = MarvelBookInfoProvider()
    if querystring is not None:
        return provider.query(querystring, page=page)
    if book.marvel_id:
        provider.get(book.marvel_id)
    if provider.total < 1 and book.isbn:
        provider.query(book.isbn, page=page)
    if provider.total < 1:
        title_words = re.findall(r'(\b\w+\b)', book.title)
        title = ' '.join(title_words[:2])
        if title_words[2].lower().startswith('vol'):
            title += ' Vol'
        provider.query(''.join(title_words[:2]) + ' Vol', page=page)
    if provider.total < 1 and provider.isbns:
        for isbn in provider.isbns:
            provider.query(isbn, page=page)
            if provider.total > 0:
                break
    if provider.total < 1:
        return provider
    return provider


def get_marvel_book(book):
    provider = get_marvel_books(book)
    title_matches = [
        x for x in provider.mapping.values()
        if x.get('title', '').lower().startswith(book.title.lower()[:15])]
    title_match = None
    if len(title_matches) > 0:
        title_match = title_matches[0]
    marvel_book = provider.mapping.get(
        book.isbn,
        title_match)
    return marvel_book


def get_collection(user):
    try:
        collection = user.collection
    except ObjectDoesNotExist:
        collection = Collection.objects.create(
            user=user)
    return collection


def get_collecting(book: Book) -> str:
    """Parse the description of a book to find the collected issues summary.
    """
    collecting = re.search(
        r'collect\w*[\s:](?P<collecting>.*)',
        book.description,
        re.IGNORECASE)
    if collecting is None:
        raise ValueError('No "collecting" statement found in "{.title}"\'s description'.format(book))
    return collecting.group('collecting')


def get_collected_issues(book: Book) -> Iterator:
    """Parse the 'collecting' part of the book description, for series names and issue details.
    """
    collecting = get_collecting(book)
    return re.finditer(FULL_REGEX, collecting, re.IGNORECASE)


def parse_description(book):
    issues_added = 0
    for collected_series in get_collected_issues(book):
        series_name = collected_series.group('series_name').strip()
        try:
            series = Series.objects.get(id=book.series_mapping[series_name.upper()])
        except Series.DoesNotExist:
            url = '%s%s' % (
                reverse('admin:books_series_changelist'),
                '?{}'.format(urlencode({'q': series_name, 'provider': 'marvel'})))
            return False, mark_safe(
                '<a href="%(url)s">%(series)s</a> does not exist yet! (click to search Marvel)' % {
                    'url': url,
                    'series': series_name})
        except KeyError:
            url = reverse('admin:update-series', kwargs={'book_id': book.id})
            return False, mark_safe(
                '<a href="{url}">{series}</a> does not exist yet! (click to search Marvel)'.format(
                    url=url,
                    series=series_name))
        series_dict = collected_series.groupdict()
        start = series_dict['start']
        start_prefix = series_dict['start_prefix']
        start_suffix = series_dict['start_suffix']
        end = series_dict['end']
        end_prefix = series_dict['end_prefix']
        end_suffix = series_dict['end_suffix']
        last_issue = 0
        if start is not None:
            first_issue = int(start)
        else:
            try:
                first_issue = int(start_suffix)
            except ValueError:
                first_issue = int(start_prefix)
        if end is not None:
            last_issue = int(end)
        elif end_suffix is not None:
            try:
                last_issue = int(end_suffix)
            except ValueError:
                first_issue = int(end_prefix)
        issue_range = range(first_issue, last_issue + 1) if last_issue > 0 else [first_issue]
        for n in issue_range:
            prefix = start_prefix if start_prefix is not None else n
            try:
                int(start_suffix)
            except (ValueError, TypeError):
                suffix = start_suffix if start_suffix is not None else ''
            else:
                suffix = n
            issue, _ = Issue.objects.get_or_create(
                number=prefix,
                series=series,
                suffix=suffix)
            if not book.issues.filter(id=issue.id).exists():
                issues_added += 1
                book.issues.add(issue)
            book.save()
    return True, 'Added {} issues to "{.title}"'.format(issues_added, book)


def add_book_to_collection(book, collection, owned=True, read=False):
    collected_book, created = CollectedBooks.objects.get_or_create(
        collection=collection,
        book=book,
        defaults={
            'owned': owned,
            'read': read})
    if not created and collected_book.read == read:
        collected_book.owned = owned
        collected_book.save()
        return True, '"{0.title}" moved to {list}'.format(
            book,
            list='collection' if owned else 'wishlist')
    elif not created:
        collected_book.read = read
        collected_book.save()
        return True, '"{0.title}" marked as {state}'.format(
            book,
            state='read' if read else 'unread')
    else:
        return True, '"{0.title}" added to {list}'.format(
            book,
            list='collection' if owned else 'wishlist')


def remove_book_from_collection(book, collection):
    collected_book = collection.collected_books.get(book__id=book.id)
    collection.collected_books.filter(book__id=book.id).delete()
    return True, '"%s" removed from %s' % (
        book.title,
        'collection' if collected_book.owned else 'wishlist')


def get_books_in_tree(collection, queryset):
    owned_book_ids = collection.books.exclude(id__in=queryset.values('id')).values('id')
    unowned_books = queryset.exclude(id__in=owned_book_ids)
    books_to_add = set()
    for book in unowned_books:
        new_books = set(book.get_family().exclude(id__in=owned_book_ids))
        books_to_add.update(new_books)
    return books_to_add


def update_book_from_marvel(request, book=None, marvel_result=None):
    if book is None and marvel_result is None:
        messages.error(
            request,
            'Must provide book or marvel result')
        return
    if marvel_result is None:
        marvel_result = get_marvel_book(book)
    if marvel_result is None:
        return
    for field in MARVEL_FIELDS:
        update_field_from_marvel(book, marvel_result, field)
    try:
        success, message = parse_description(book)
    except ValueError as ex:
        success = False
        message = ex.args[0]
    if not success:
        messages.warning(
            request,
            message
        )


def add_series_to_book(book, series, original_upper=None):
    if not book.series.filter(id=series.id).exists():
        book.series.add(series)
    if original_upper is not None:
        book.series_mapping[original_upper] = str(series.id)
    book.save()


def check_series_exist(book: Book, series_names: list) -> Tuple[bool, dict]:
    all_found = True
    series_data = {}
    for series_name in series_names:
        series_id = book.series_mapping.get(series_name.upper())
        lookup = {'id': series_id} if series_id is not None else {'name__iexact': series_name}
        try:
            series_data[series_name] = Series.objects.get(**lookup)
        except Series.DoesNotExist:
            all_found = False
            series_data[series_name] = None
        else:
            add_series_to_book(book, series_data[series_name], series_name.upper())
    book.save()
    return all_found, series_data
