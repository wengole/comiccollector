book_bindings = (('hardcover', 'Hardcover'),
                 ('paperback', 'Paperback'))
book_types = (
    ('trade paperback', 'Trade Paperback (TPB)'),
    ('hardcover', 'Hardcover (HC)'),
    ('omnibus', 'Omnibus'),
    ('event', 'Event Book'),
    ('comic', 'Comic (Single issue)')
)

related_reasons = (('soft', 'Useful pre-reading'),
                   ('hard', 'Required pre-reading'))

collection_actions = (
    ('add-collection', 'Add to collection'),
    ('remove-collection', 'Remove from collection'),
    ('add-wishlist', 'Add to wishlist'),
    ('remove-wishlist', 'Remove from wishlist'),
    ('add-read', 'Mark as read'),
    ('remove-read', 'Mark as unread')
)

image_source = (
    ('other', 'Other'),
    ('amazon', 'Amazon'),
    ('marvel', 'Marvel'),
)
