$('#book_dialog').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var book_id = button.data('book-id');
    var book_state = button.data('book-state');
    var book_url = button.data('book-url');
    var book_title = button.data('book-title');
    var modal = $(this);
    var collection_button = modal.find('button#collection');
    var wishlist_button = modal.find('button#wishlist');
    modal.find('.modal-title').text(book_title);
    modal.find('input[name=book]').val(book_id);
    modal.find('a#detail').attr('href', book_url);
    var read_button = modal.find('button#read');
    if (book_state == 'unowned' || book_state == 'wanted') {
        collection_button.val('add-collection');
        collection_button.find('.fa:nth-child(1):not(.fa-bookmark-o)').addClass('fa-bookmark-o');
        collection_button.find('.fa:nth-child(2):not(.fa-bookmark)').addClass('fa-bookmark');
        collection_button.find('span').text(' Add to collection');
    } else {
        collection_button.val('remove-collection');
        collection_button.find('.fa:nth-child(1):not(.fa-bookmark)').addClass('fa-bookmark');
        collection_button.find('.fa:nth-child(2):not(.fa-bookmark-o)').addClass('fa-bookmark-o');
        collection_button.find('span').text(' Remove from collection');
    }
    if (book_state != 'wanted') {
        wishlist_button.val('add-wishlist');
        wishlist_button.find('.fa:nth-child(1):not(.fa-star-o)').addClass('fa-star-o');
        wishlist_button.find('.fa:nth-child(2):not(.fa-star)').addClass('fa-star');
        wishlist_button.find('span').text(' Add to wishlist');
    } else {
        wishlist_button.val('remove-wishlist');
        wishlist_button.find('.fa:nth-child(1):not(.fa-star)').addClass('fa-star');
        wishlist_button.find('.fa:nth-child(2):not(.fa-star-o)').addClass('fa-star-o');
        wishlist_button.find('span').text(' Remove from wishlist');
    }
    if (book_state != 'read') {
        read_button.val('add-read');
        read_button.find('.fa:nth-child(1):not(.fa-square-o)').addClass('fa-square-o');
        read_button.find('.fa:nth-child(2):not(.fa-check-square-o)').addClass('fa-check-square-o');
        read_button.find('span').text(' Mark as read');
    } else {
        read_button.val('remove-read');
        read_button.find('.fa:nth-child(1):not(.fa-check-square-o)').addClass('fa-check-square-o');
        read_button.find('.fa:nth-child(2):not(.fa-square-o)').addClass('fa-square-o');
        read_button.find('span').text(' Mark as unread');
    }
});
