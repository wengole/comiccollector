from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from books import views as books_views

urlpatterns = [
    url(r'^accounts/profile/$',
        books_views.UserProfileView.as_view(),
        name='user-profile'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'author-autocomplete/$',
        books_views.AuthorAutocomplete.as_view(),
        name='author-autocomplete'),
    url(r'issue-autocomplete/$',
        books_views.IssueAutocomplete.as_view(),
        name='issue-autocomplete'),
    url(r'book-autocomplete/$',
        books_views.BookAutoComplete.as_view(),
        name='book-autocomplete'),
    url(r'series-autocomplete',
        books_views.SeriesAutocomplete.as_view(),
        name='series-autocomplete'),
    url(r'^$',
        books_views.HomePageView.as_view(),
        name='home-page'),
    url(r'^comic/(?P<book_id>\d+)/$',
        books_views.BookDetailView.as_view(),
        name='comic-detail'),
    url(r'^collection/$',
        books_views.CollectionView.as_view(),
        name='collection'),
    url(r'^search/$',
        books_views.SearchView.as_view(),
        name='search'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
