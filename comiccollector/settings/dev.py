from .base import *

INSTALLED_APPS += [
    'debug_toolbar',
    'template_profiler_panel',
]

MIDDLEWARE_CLASSES += [
    'yet_another_django_profiler.middleware.ProfilerMiddleware',
]

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    # 'debug_toolbar.panels.settings.SettingsPanel',
    # 'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    # 'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    # 'debug_toolbar.panels.cache.CachePanel',
    # 'debug_toolbar.panels.signals.SignalsPanel',
    # 'debug_toolbar.panels.logging.LoggingPanel',
    # 'debug_toolbar.panels.redirects.RedirectsPanel',
    'template_profiler_panel.panels.template.TemplateProfilerPanel',
]

DEBUG_TOOLBAR_CONFIG = {
    'RENDER_PANELS': False,
    'RESULTS_CACHE_SIZE': 1,
    'ENABLE_STACKTRACES': False,
}
